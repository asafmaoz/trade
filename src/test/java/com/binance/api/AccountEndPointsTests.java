package com.binance.api;

import com.binance.api.client.BinanceApiRestClient;
import com.binance.api.client.domain.account.Account;
import com.binance.api.client.domain.account.Order;
import com.binance.api.client.domain.account.Trade;
import com.binance.api.client.domain.account.WithdrawHistory;
import com.binance.api.client.domain.account.request.AllOrdersRequest;
import com.binance.api.client.domain.account.request.CancelOrderRequest;
import com.binance.api.client.domain.account.request.OrderRequest;
import com.binance.api.client.domain.account.request.OrderStatusRequest;
import com.binance.api.client.exception.BinanceApiException;
import com.binance.api.client.impl.BinanceApiRestClientImpl;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

@RunWith(MockitoJUnitRunner.class)
@Ignore
public class AccountEndPointsTests {
    BinanceApiRestClient binanceApiRestClient;
    String apiKey = "9Z6aUwyrjY0ME9k6bqraVxNtNTTfAryuy5zr45U3AzHpaDyrZXH2NDeLEBsy8B77";
    String apiSecret = "phAaPn1cEnQSnqe5OpKWlnu5eCLO6dDLBqJ27Q3qTBG1rlszs1HWN6mxz854dhRF";
    static String SYMBOL = "XRPBTC";
    Integer limit = 10;
    OrderStatusRequest orderStatusRequest;

    @Before
    public void setUp(){
        this.binanceApiRestClient = new BinanceApiRestClientImpl(this.apiKey, this.apiSecret);
        this.orderStatusRequest = new OrderStatusRequest(SYMBOL,5L);
    }

    @Test(expected = BinanceApiException.class)
    public void orderIdInvalid_ApiException() {
        Order order = this.binanceApiRestClient.getOrderStatus(this.orderStatusRequest);
        assertThat(null, is(equalTo(null)));
    }

    @Test
    public void getAllOrdersAndGetOrderStatus() {
        AllOrdersRequest allOrdersRequest = new AllOrdersRequest(SYMBOL);
        List<Order> orders = this.binanceApiRestClient.getAllOrders(allOrdersRequest);
        assertThat(orders.size() > 0, is(true));
        Long orderId = orders.get(orders.size()-1).getOrderId();
        this.orderStatusRequest = new OrderStatusRequest(SYMBOL,orderId);
        Order order = this.binanceApiRestClient.getOrderStatus(this.orderStatusRequest);
        assertThat(order.getSymbol(), is(equalTo(SYMBOL)));
    }

    @Test
    public void getOpenOrders() {
        OrderRequest orderRequest = new OrderRequest(SYMBOL);
        List<Order> orders = this.binanceApiRestClient.getOpenOrders(orderRequest);
    }



    

    @Ignore
    @Test
    public void cancelOrder(){
        CancelOrderRequest cancelOrderRequest = new CancelOrderRequest(SYMBOL,25990458L);
        this.binanceApiRestClient.cancelOrder(cancelOrderRequest);
    }

    @Test
    public void getAccount(){
        Account account = this.binanceApiRestClient.getAccount();
        System.out.println(account);
        StringBuilder as = new StringBuilder();

        account.getBalances().forEach(assetBalance -> {
            as.append(assetBalance.getAsset()+',');
            if(Float.parseFloat(assetBalance.getFree()) > 0)
                System.out.println(assetBalance.getAsset()+" "+assetBalance.getFree());
        });
        assertThat(account.getBalances().size() > 0, is(true));
    }

    @Test
    public void getMyTraders() {
        List<Trade> trades = this.binanceApiRestClient.getMyTrades(SYMBOL,this.limit);
        System.out.println(trades);
    }

    @Test
    public void getWithdrawHistory() {
        WithdrawHistory withdrawHistoryBTC = this.binanceApiRestClient.getWithdrawHistory("BTC");

        WithdrawHistory withdrawHistoryETH = this.binanceApiRestClient.getWithdrawHistory("ETH");
        assertThat(null, is(equalTo(null)));
    }
}
