package com.binance.api;
import com.binance.api.client.impl.BinanceApiRestClientImpl;
import com.autoexchange.models.AccountPerformanceModel;
import com.autoexchange.models.AccountProperties;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
@Ignore
public class AccountPerformanceModelTests {
    AccountPerformanceModel accountPerformanceModel;
    AccountProperties accountProperties = new AccountProperties();


    @Before
    public void setUp() throws Exception {
        this.accountPerformanceModel = new AccountPerformanceModel();
        accountProperties.setUserId("asaf");
        this.accountPerformanceModel.binanceApiRestClient = new BinanceApiRestClientImpl("9Z6aUwyrjY0ME9k6bqraVxNtNTTfAryuy5zr45U3AzHpaDyrZXH2NDeLEBsy8B77",
                "phAaPn1cEnQSnqe5OpKWlnu5eCLO6dDLBqJ27Q3qTBG1rlszs1HWN6mxz854dhRF");
    }
    @Test
    public void test_getCurrentAccountStatus(){
        System.out.println(this.accountPerformanceModel.getAccountPerformance());
    }

    @Test
    public void test_calcSummaryForPair(){
        System.out.println(this.accountPerformanceModel.calcSummaryForPair("SNT", "BTC"));
    }
}
