package com.binance.api;

import com.binance.api.client.BinanceApiRestClient;
import com.binance.api.client.domain.general.ExchangeInfo;
import com.binance.api.client.impl.BinanceApiRestClientImpl;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

@RunWith(MockitoJUnitRunner.class)
@Ignore
public class GeneralEndPointsTests {
    BinanceApiRestClient binanceApiRestClient;
    String apiKey = "testGeneralApiKey";
    String apiSecret = "testGeneralApiSecret";

    @Before
    public void setUp(){
        this.binanceApiRestClient = new BinanceApiRestClientImpl(this.apiKey, this.apiSecret);
    }
    @Test
    public void testPing(){
        this.binanceApiRestClient.ping();
    }

    @Test
    public void getServerTime() {
        Long serverTime = this.binanceApiRestClient.getServerTime();
        assertThat(serverTime>0, is(true));
    }

    @Test
    public void getExchangeInfo() {
        ExchangeInfo exchangeInfo = this.binanceApiRestClient.getExchangeInfo();
        assertThat(exchangeInfo.getServerTime()>0, is(true));
        assertThat(exchangeInfo.getSymbols().size()>0, is(true));
        assertThat(exchangeInfo.getSymbolInfo("ETHBTC").getBaseAsset(), is(equalTo("ETH")));
    }
}
