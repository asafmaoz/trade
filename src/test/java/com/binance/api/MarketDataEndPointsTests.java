package com.binance.api;

import com.binance.api.client.BinanceApiRestClient;
import com.binance.api.client.domain.market.AggTrade;
import com.binance.api.client.domain.market.OrderBook;
import com.binance.api.client.domain.market.TickerStatistics;
import com.binance.api.client.impl.BinanceApiRestClientImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;


@RunWith(MockitoJUnitRunner.class)
@Ignore
public class MarketDataEndPointsTests {
    BinanceApiRestClient binanceApiRestClient;
    String apiKey = "testGeneralApiKey";
    String apiSecret = "testGeneralApiSecret";
    static String SYMBOL = "XRPBTC";
    Integer limit = 10;

    @Before
    public void setUp(){
        this.binanceApiRestClient = new BinanceApiRestClientImpl(this.apiKey, this.apiSecret);
    }

    @Test
    public void getOrderBook(){
        OrderBook orderBook = this.binanceApiRestClient.getOrderBook(SYMBOL, this.limit);
        assertThat(orderBook.getLastUpdateId()>0, is(true));
        assertThat(orderBook.getBids().size(), is(this.limit));
    }

    @Test
    public void getAggTrades() {
        List<AggTrade> aggTrades = this.binanceApiRestClient.getAggTrades(SYMBOL,null,this.limit,null,null);
        System.out.println(aggTrades);
        assertThat(Float.parseFloat(aggTrades.get(0).getPrice())>0,is(true));
        assertThat(aggTrades.size(), is(this.limit));
    }

    @Test
    public void get24HrPriceStatistics() {
        TickerStatistics tickerStatistics = this.binanceApiRestClient.get24HrPriceStatistics(SYMBOL);
        Assert.assertNotNull(tickerStatistics.getPriceChangePercent());
        Assert.assertNotNull(tickerStatistics.getLastPrice());
        Assert.assertNotNull(tickerStatistics.getHighPrice());
        Assert.assertNotNull(tickerStatistics.getLowPrice());
        Assert.assertThat(tickerStatistics.getCount()>0,is(true));
    }
}
