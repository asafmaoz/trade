package com.autoexchange.models;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class StopLossTest {

    StopLoss stopLoss;
    @Before
    public void setUp() throws Exception {
        this.setDefaultParams();

    }

    private void setDefaultParams() throws Exception {
        this.stopLoss = new StopLoss();
        this.stopLoss.setBuyPrice(10f);
        this.stopLoss.setPercentDiff(10f);
    }

    @Test
    public void defaultValues_calcStopLossRate() {
        this.stopLoss.calcRate();
        assertThat(this.stopLoss.getStopLossRate(), is(equalTo(9f)));
    }

    @Test (expected = Exception.class)
    public void overMaxPercentDiff() throws Exception {
        this.stopLoss.setPercentDiff(110f);
        this.stopLoss.calcRate();
    }

    @Test (expected = Exception.class)
    public void underMinPercentDiff() throws Exception {
        this.stopLoss.setPercentDiff(-4f);
        this.stopLoss.calcRate();
    }

    @After
    public void tearDown() throws Exception {
    }
}