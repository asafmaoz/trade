package com.autoexchange.persistence;

import com.autoexchange.persistence.repositories.ExchangeRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

@RunWith(MockitoJUnitRunner.class)
public class ExchangeTest {
    @Autowired
    ExchangeRepository exchangeRepository;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void name() {
        ExchangeDAO exchangeDAO = new ExchangeDAO("binance");
        exchangeRepository.getExchangeNameById(1L);
        exchangeRepository.save(new ExchangeDAO("binance"));
//        System.out.println(this.exchangeDAO.g);
        assertThat(null, is(equalTo(null)));
    }
}
