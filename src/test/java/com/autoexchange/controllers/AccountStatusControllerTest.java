package com.autoexchange.controllers;

import com.autoexchange.models.AccountProperties;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class AccountStatusControllerTest {

    AccountStatusController accountStatusController;
    Map<String, Object> requestModel;
    @Mock
    AccountProperties accountProperties;

    @Before
    public void setUp() throws Exception {
        this.accountStatusController = new AccountStatusController();
        doReturn("9Z6aUwyrjY0ME9k6bqraVxNtNTTfAryuy5zr45U3AzHpaDyrZXH2NDeLEBsy8B77").when(this.accountProperties).getApiKey();
        doReturn("phAaPn1cEnQSnqe5OpKWlnu5eCLO6dDLBqJ27Q3qTBG1rlszs1HWN6mxz854dhRF").when(this.accountProperties).getApiSecret();
        accountStatusController.accountProperties = accountProperties;
        requestModel = new HashMap<>();
    }

    @Test
    public void missingUserId_exception() throws Exception {
        this.accountStatusController.getAccountPerformanceSummary("");
    }
}