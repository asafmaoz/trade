package com.autoexchange.controllers;

import com.autoexchange.models.AccountProperties;
import com.autoexchange.models.PairPerformanceSummary;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class TradeControllerTest {

    Map<String, Object> input = new HashMap<>();
    TradeController tradeController;
    @Mock
    AccountProperties accountProperties;
    @Before
    public void setUp() throws Exception {
        tradeController = new TradeController();
        doReturn("9Z6aUwyrjY0ME9k6bqraVxNtNTTfAryuy5zr45U3AzHpaDyrZXH2NDeLEBsy8B77").when(this.accountProperties).getApiKey();
        doReturn("phAaPn1cEnQSnqe5OpKWlnu5eCLO6dDLBqJ27Q3qTBG1rlszs1HWN6mxz854dhRF").when(this.accountProperties).getApiSecret();
        tradeController.accountProperties = accountProperties;
        input.put("userId", "asaf");
        input.put("base", "BTC");
        input.put("asset", "GTO");

    }

    @Test
    public void getPairTradeSummary() throws Exception {
        PairPerformanceSummary pairPerformanceSummary = this.tradeController.getPairTradeSummary(this.input, (String) this.input.get("userId"));
        System.out.println(pairPerformanceSummary);
    }
}