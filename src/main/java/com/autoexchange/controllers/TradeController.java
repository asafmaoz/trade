package com.autoexchange.controllers;

import com.binance.api.client.BinanceApiClientFactory;
import com.binance.api.client.BinanceApiRestClient;
import com.binance.api.client.domain.account.Trade;
import com.autoexchange.models.AccountProperties;
import com.autoexchange.models.PairPerformanceSummary;
import com.autoexchange.models.PairTradePerformanceModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class TradeController {

    BinanceApiRestClient binanceApiRestClient;

    @Autowired
    AccountProperties accountProperties;

    PairTradePerformanceModel pairTradePerformanceModel = new PairTradePerformanceModel();

    @PostMapping(value = "/pairTradeSummary")
    public PairPerformanceSummary getPairTradeSummary(@RequestBody Map<String, Object> model, @RequestHeader String userId) throws Exception {
        this.accountProperties.setUserId(userId);
        BinanceApiClientFactory factory = BinanceApiClientFactory.newInstance(this.accountProperties.getApiKey(),
                this.accountProperties.getApiSecret());
        this.binanceApiRestClient = factory.newRestClient();
        this.pairTradePerformanceModel.setPair((String) model.get("asset") + model.get("base"));
        this.pairTradePerformanceModel.setAsset((String)model.get("asset"));
        this.pairTradePerformanceModel.binanceApiRestClient = this.binanceApiRestClient;
        PairPerformanceSummary pairPerformanceSummary = this.pairTradePerformanceModel.getPairTradeSummary();
        pairPerformanceSummary.setBase((String)model.get("base"));
        return pairPerformanceSummary;
    }

    @GetMapping("/getPairTrades")
    public List<Trade> getPairTrades(@RequestParam String asset, @RequestParam String base, @RequestHeader String userId) throws Exception {
        this.accountProperties.setUserId(userId);
        BinanceApiClientFactory factory = BinanceApiClientFactory.newInstance(this.accountProperties.getApiKey(),
                this.accountProperties.getApiSecret());
        this.binanceApiRestClient = factory.newRestClient();
        List<Trade> myTrades = this.binanceApiRestClient.getMyTrades(asset + base);

        return myTrades;
    }
}
