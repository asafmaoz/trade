package com.autoexchange.controllers;
import com.binance.api.client.BinanceApiClientFactory;
import com.binance.api.client.BinanceApiRestClient;
import com.autoexchange.models.AccountPerformanceModel;
import com.autoexchange.models.AccountProperties;
import com.autoexchange.models.PairPerformanceSummary;
import com.autoexchange.responses.AccountPerformanceSummary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AccountStatusController {

    BinanceApiRestClient binanceApiRestClient;

    @Autowired
    AccountProperties accountProperties;

    AccountPerformanceModel accountPerformanceModel;

    @PostMapping("/accountPerformance")
    public List<AccountPerformanceSummary> getAccountPerformanceSummary(@RequestHeader String userId) throws Exception {
        this.accountProperties.setUserId(userId);
        BinanceApiClientFactory factory = BinanceApiClientFactory.newInstance(this.accountProperties.getApiKey(),
                this.accountProperties.getApiSecret());
        this.binanceApiRestClient = factory.newRestClient();

        this.accountPerformanceModel = new AccountPerformanceModel();
        this.accountPerformanceModel.binanceApiRestClient = this.binanceApiRestClient;
        this.accountPerformanceModel.accountProperties = this.accountProperties;
        List<PairPerformanceSummary> pairPerformanceSummaryList = this.accountPerformanceModel.getAccountPerformance();
        List<AccountPerformanceSummary> accountPerformanceSummary = this.accountPerformanceModel.calcSummary(pairPerformanceSummaryList);

        return accountPerformanceSummary;
    }

}
