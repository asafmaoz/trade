package com.autoexchange.controllers;


import com.autoexchange.persistence.ExchangeDAO;
import com.autoexchange.persistence.repositories.ExchangeRepository;
import com.autoexchange.responses.GeneralResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.Map;

@RestController
public class DataScraperController {

    GeneralResponse generalResponse;

    @Autowired
    ExchangeRepository exchangeRepository;

    @PostConstruct
    public void initResponse(){
        generalResponse = new GeneralResponse(false, "init");
    }

    @PostMapping("/updateOrders")
    public GeneralResponse updateOrders(@RequestHeader String userId, @RequestBody Map<String, Object> model){

        return this.generalResponse;
    }
//
//    @PostMapping(name = "/addAccount")
//    public GeneralResponse addAccount(@RequestHeader String accessKey, @RequestHeader String secretKey,
//                                      @RequestHeader String exchangeId, @RequestHeader String userIdentifier){
//
//        return this.generalResponse;
//    }


    @PostMapping(name = "/addExchange")
    public GeneralResponse addExchange(@RequestHeader String exchangeName){
        ExchangeDAO exchangeDAO = new ExchangeDAO(exchangeName);
        exchangeRepository.save(exchangeDAO);
        return this.generalResponse.withSuccessDetails("cool");
    }
}
