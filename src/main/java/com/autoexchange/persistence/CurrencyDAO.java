package com.autoexchange.persistence;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "currency")
@Data public class CurrencyDAO {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String currencyName;
    private boolean isBaseCurrency;
    private Long exchangeId;
}
