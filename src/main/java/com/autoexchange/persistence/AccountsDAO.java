package com.autoexchange.persistence;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "accounts")
@Data public class AccountsDAO {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String accessKey;
    private String secretKey;
    private int exchangeId;
    private String userIdentifier;
    @CreationTimestamp
    private Timestamp creationTimeStamp;
}
