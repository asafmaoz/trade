package com.autoexchange.persistence;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "deposits_withdrawals")
@Data public class DepositsWithdrawalsDAO {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String asset;
    private boolean isDeposit;
    private Long exchangeId;
    private String address;
    private String transactionId;
    private String fee;
}
