package com.autoexchange.persistence;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "exchange")
@Data
public class ExchangeDAO {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String exchangeName;
    @CreationTimestamp
    private Timestamp creationTimeStamp;

    ExchangeDAO(){}
    public ExchangeDAO(String exchangeName) {
        this.exchangeName = exchangeName;
    }
}
