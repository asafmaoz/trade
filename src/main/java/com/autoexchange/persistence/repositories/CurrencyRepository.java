package com.autoexchange.persistence.repositories;

import com.autoexchange.persistence.CurrencyDAO;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CurrencyRepository extends JpaRepository<CurrencyDAO, Long>{
}
