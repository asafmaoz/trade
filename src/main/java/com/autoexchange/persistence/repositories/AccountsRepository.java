package com.autoexchange.persistence.repositories;

import com.autoexchange.persistence.AccountsDAO;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountsRepository extends JpaRepository<AccountsDAO, Long> {
}
