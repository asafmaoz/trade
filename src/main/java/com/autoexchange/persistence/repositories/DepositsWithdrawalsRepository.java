package com.autoexchange.persistence.repositories;

import com.autoexchange.persistence.DepositsWithdrawalsDAO;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepositsWithdrawalsRepository extends JpaRepository<DepositsWithdrawalsDAO, Long> {
}
