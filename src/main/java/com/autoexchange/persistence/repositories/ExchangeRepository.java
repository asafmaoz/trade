package com.autoexchange.persistence.repositories;


import com.autoexchange.persistence.ExchangeDAO;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExchangeRepository extends JpaRepository<ExchangeDAO, Long> {
    String getExchangeNameById(Long exchangeId);
}
