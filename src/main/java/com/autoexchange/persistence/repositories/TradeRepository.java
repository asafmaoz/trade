package com.autoexchange.persistence.repositories;
import com.binance.api.client.domain.account.Trade;
import com.autoexchange.persistence.TradeDAO;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TradeRepository extends JpaRepository<TradeDAO, Long> {
    List<Trade> findByAssetAndBase(String asset, String base);
}
