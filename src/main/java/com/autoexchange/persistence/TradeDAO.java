package com.autoexchange.persistence;


import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "trade")
@Data
public class TradeDAO {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @CreationTimestamp
    private Timestamp creationTimeStamp;

    @UpdateTimestamp
    private Timestamp updateTimeStamp;

    private int exchangeId;
    private Long tradeId;
    private long tradeTime;
    private String asset;
    private String base;
    private Long orderId;
    private String pricePerUnit;
    private String quantity;
    private String commission;
    private String commissionAsset;
    private boolean buyTrade;
    private String tradeCost;


    protected TradeDAO(){}


}
