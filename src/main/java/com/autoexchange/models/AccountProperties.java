package com.autoexchange.models;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class AccountProperties {
    private String userId;

    @Autowired
    private Environment env;

    private List<String> allCurrencies;
    private List<String> allBases;


    public String getApiKey() throws Exception {
        String apiKey = env.getProperty("api_key_"+userId);
        if(StringUtils.isBlank(apiKey))
            throw new Exception("USERID does not match any property");
        return apiKey;
    }

    public String getApiSecret() throws Exception {
        String apiSecret = env.getProperty("api_secret_"+userId);
        if(StringUtils.isBlank(apiSecret))
            throw new Exception("USERID does not match any property");
        return apiSecret;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<String> getAllCurrencies() {
        this.allCurrencies = Arrays.asList(env.getProperty("all_currencies_default").split(","));
        return allCurrencies;
    }

    public void setAllCurrencies() {
        this.allCurrencies = Arrays.asList(env.getProperty("all_currencies_default").split(","));
    }

    public List<String> getAllBases() {
        this.allBases = Arrays.asList(env.getProperty("all_base_currencies").split(","));
        return allBases;
    }

    public void setAllBases() {
        this.allBases = Arrays.asList(env.getProperty("all_base_currencies").split(","));
    }
}
