package com.autoexchange.models;

import org.springframework.stereotype.Service;

@Service
public class PairPerformanceSummary {
    private Float assetBought = 0f;
    private Float assetSold = 0f;
    private String pair;
    private Float bottomLine = 0f;
    private String base;
    private Float currentBalanceInBTC = 0f;

    public void setCurrentBalanceInBTC(Float currentBalanceInBTC) {
        this.currentBalanceInBTC = currentBalanceInBTC;
    }


    public Float getAssetBought() {
        return assetBought;
    }

    public void setAssetBought(Float assetBought) {
        this.assetBought = assetBought;
    }

    public Float getAssetSold() {
        return assetSold;
    }

    public void setAssetSold(Float assetSold) {
        this.assetSold = assetSold;
    }

    @Override
    public String toString() {
        return "PairPerformanceSummary{" +
                "assetBought=" + assetBought +
                ", assetSold=" + assetSold +
                ", pair='" + pair + '\'' +
                ", bottomLine=" + bottomLine +
                ", base='" + base + '\'' +
                ", currentBalanceInBTC=" + currentBalanceInBTC +
                '}';
    }

    public String getPair() {
        return pair;
    }

    public void setPair(String pair) {
        this.pair = pair;
    }

    public Float getBottomLine() {
        return bottomLine;
    }

    public void setBottomLine(Float bottomLine) {
        this.bottomLine = bottomLine;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public Float getCurrentBalanceInBTC() {
        return currentBalanceInBTC;
    }
}
