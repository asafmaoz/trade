package com.autoexchange.models;

import com.binance.api.client.BinanceApiRestClient;
import com.binance.api.client.domain.account.Account;
import com.binance.api.client.domain.account.Trade;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class PairTradePerformanceModel {

    @Autowired
    public BinanceApiRestClient binanceApiRestClient;

    @Autowired
    public AccountProperties accountProperties;

    private String pair;
    private String asset;

    private PairPerformanceSummary calcPairPerformanceSummary(){
        PairPerformanceSummary pairPerformanceSummary = new PairPerformanceSummary();
        pairPerformanceSummary.setPair(this.pair);
        List<Trade> myTrades = this.binanceApiRestClient.getMyTrades(this.pair);
        myTrades.forEach(trade -> {
            if(trade.isBuyer()){
                pairPerformanceSummary.setAssetBought(pairPerformanceSummary.getAssetBought()+
                Float.parseFloat(trade.getQty())*Float.parseFloat(trade.getPrice()));
            }else{
                pairPerformanceSummary.setAssetSold(pairPerformanceSummary.getAssetSold()+
                        Float.parseFloat(trade.getQty())*Float.parseFloat(trade.getPrice()));
            }
        });
        pairPerformanceSummary.setCurrentBalanceInBTC(this.getCurrentPairBalance());
        pairPerformanceSummary.setBottomLine(pairPerformanceSummary.getCurrentBalanceInBTC() +
                pairPerformanceSummary.getAssetSold() -
                pairPerformanceSummary.getAssetBought());
        return pairPerformanceSummary;
    }

    private Float getCurrentPairBalance(){
        Account account = this.binanceApiRestClient.getAccount();
        Float currentBalance = Float.parseFloat(account.getAssetBalance(this.asset).getFree())+
                Float.parseFloat(account.getAssetBalance(this.asset).getLocked());

        String lastPrice = this.binanceApiRestClient.get24HrPriceStatistics(this.pair)
                .getLastPrice();
        return currentBalance * Float.parseFloat(lastPrice);
    }

    public PairPerformanceSummary getPairTradeSummary(){
        return this.calcPairPerformanceSummary();
    }

    public void setPair(String pair) {
        this.pair = pair;
    }

    public void setAsset(String asset) {
        this.asset = asset;
    }


}
