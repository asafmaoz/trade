package com.autoexchange.models;

import com.binance.api.client.BinanceApiRestClient;
import com.binance.api.client.domain.account.Account;
import com.binance.api.client.domain.account.AssetBalance;
import com.binance.api.client.domain.account.Trade;
import com.autoexchange.responses.AccountPerformanceSummary;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class AccountPerformanceModel {

    @Autowired
    public BinanceApiRestClient binanceApiRestClient;

    @Autowired
    public AccountProperties accountProperties;


    public List<PairPerformanceSummary> getAccountPerformance(){
        List<PairPerformanceSummary> pairPerformanceSummaryList = new ArrayList<>();
        accountProperties.getAllCurrencies().forEach(currency->{
            accountProperties.getAllBases().forEach(base->{
                PairPerformanceSummary pairPerformanceSummary = this.calcSummaryForPair(currency,base);
                if(pairPerformanceSummary.getBottomLine() != 0 || pairPerformanceSummary.getAssetBought() != 0 || pairPerformanceSummary.getAssetSold() != 0)
                    pairPerformanceSummaryList.add(this.calcSummaryForPair(currency,base));
            });
        });
        return pairPerformanceSummaryList;
    }

    public Float getCurrentAccountBTCstatus(){
        Float btcBalance = 0f;
        Account account = this.binanceApiRestClient.getAccount();
        for (int i = 0; i < account.getBalances().size(); i++) {
            AssetBalance assetBalance = account.getBalances().get(i);
            if(Float.parseFloat(assetBalance.getFree()) + Float.parseFloat(assetBalance.getLocked()) < 1)
                continue;
            if(assetBalance.getAsset().equals("BTC") || assetBalance.getAsset().equals("USDT"))
                continue;
            System.out.println(assetBalance.toString());
            String lastPrice = this.binanceApiRestClient.get24HrPriceStatistics(assetBalance.getAsset()+"BTC")
                    .getLastPrice();
            btcBalance += (Float.parseFloat(assetBalance.getFree()) +
                    Float.parseFloat(assetBalance.getLocked())) * Float.parseFloat(lastPrice);
        }
        return btcBalance;
    }

    public List<AccountPerformanceSummary> calcSummary(List<PairPerformanceSummary> pairPerformanceSummaryList){
        List<AccountPerformanceSummary> accountPerformanceSummaryList = new ArrayList<>();
        accountProperties.getAllBases().forEach(base->{
            AccountPerformanceSummary accountPerformanceSummary = new AccountPerformanceSummary();
            accountPerformanceSummary.setCurrency(base);
            pairPerformanceSummaryList.forEach(pairPerformanceSummary -> {
                if(base.equals(pairPerformanceSummary.getBase()))
                    accountPerformanceSummary.setProfitSummary(
                            accountPerformanceSummary.getProfitSummary()+pairPerformanceSummary.getBottomLine());
            });
            if(accountPerformanceSummary.getCurrency().equals("BTC"))
                accountPerformanceSummary.setProfitSummary(accountPerformanceSummary.getProfitSummary()+this.getCurrentAccountBTCstatus());
            accountPerformanceSummaryList.add(accountPerformanceSummary);
        });
       return accountPerformanceSummaryList;
    }

    public PairPerformanceSummary calcSummaryForPair(String currency, String baseAsset){
        PairPerformanceSummary pairPerformanceSummary = new PairPerformanceSummary();
        pairPerformanceSummary.setPair(currency + baseAsset);
        pairPerformanceSummary.setBase(baseAsset);
        List<Trade> trades = null;

        try {
            trades = this.binanceApiRestClient.getMyTrades(pairPerformanceSummary.getPair());
        } catch (Exception e) {
            return pairPerformanceSummary;
        }

        trades.forEach(trade->{
            if(trade.isBuyer())
                pairPerformanceSummary.setAssetBought(pairPerformanceSummary.getAssetBought()+((Float.parseFloat (trade.getQty()))*(Float.parseFloat(trade.getPrice()))));
            else
                pairPerformanceSummary.setAssetSold(pairPerformanceSummary.getAssetSold()+((Float.parseFloat (trade.getQty()))*(Float.parseFloat(trade.getPrice()))));
        });
        pairPerformanceSummary.setBottomLine(pairPerformanceSummary.getAssetSold() - pairPerformanceSummary.getAssetBought());

        return pairPerformanceSummary;
    }


}
