package com.autoexchange.responses;

import lombok.Data;

@Data public class GeneralResponse {
    private boolean isRequestSuccess;
    private String message;

    public GeneralResponse withSuccessDetails(String message){
        this.setRequestSuccess(true);
        this.setMessage(message);
        return this;
    }
    public GeneralResponse(boolean requestSucceed, String message){
        this.setRequestSuccess(requestSucceed);
        this.setMessage(message);
    }
    public GeneralResponse(){}
}
