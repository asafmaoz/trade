package com.autoexchange.responses;

public class AccountPerformanceSummary {
    private String currency;
    private Float profitSummary = 0f;

    @Override
    public String toString() {
        return "AccountPerformanceSummary{" +
                "currency='" + currency + '\'' +
                ", profitSummary=" + profitSummary +
                '}';
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Float getProfitSummary() {
        return profitSummary;
    }

    public void setProfitSummary(Float profitSummary) {
        this.profitSummary = profitSummary;
    }


}
