package com.binance.api.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExcapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExcapiApplication.class, args);
	}
}
